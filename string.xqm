module namespace _ = "utils/string";

declare variable $_:newline := "&#10;";

declare function _:pad($str as xs:string, $size as xs:integer) as xs:string {
	_:pad($str, " ", $size)
};

declare function _:pad($str as xs:string, $padding as xs:string, $size as xs:integer) as xs:string {
	$str || _:repeat($padding, $size - fn:string-length($str))
};

declare function _:repeat($str as xs:string, $times as xs:integer) as xs:string {
	if ($times gt 0) then
		string-join((1 to $times) ! $str)
	else
		""
};

declare function _:trim($str as xs:string) as xs:string
{
	replace($str, "^\p{Z}+|\p{Z}+$", "")
};

declare function _:excerpt($str as xs:string, $max-size as xs:integer) as xs:string
{
	replace(fold-left(analyze-string($str, "[\p{Z}\n]+")/*, "", function ($result as xs:string, $m as element()) as xs:string {
		let $append := typeswitch ($m)
			case element(fn:match) return " "
			case element(fn:non-match) return $m/text()
			default return ""
		let $next := $result || $append
		return
			if (string-length($next) ge $max-size)
			then _:pad($result, $_:newline, $max-size)
			else $next
	}), "\n+$", "")
};

declare function _:length($nodes as node()*) as xs:integer
{
	fn:string-length(<_>{$nodes}</_>)
};


declare function _:except($str as xs:string*, $except as xs:string*) as xs:string*
{
	$str[not(. = $except)]
};

declare function _:normalize-space($nodes as node()*) as xs:string
{
	string-join(<_>{$nodes}</_>//text() ! normalize-space(.))
};
