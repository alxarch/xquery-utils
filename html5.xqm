module namespace _ = "utils/html5";

import module namespace str = "utils/string" at "string.xqm";

declare default element namespace "http://www.w3.org/1999/xhtml";

(:~
 : Sectioning root
 :
 : @see https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Sections_and_Outlines_of_an_HTML5_document#Sectioning_roots
 :)
declare variable $_:sectioning-root-elements as xs:string* := (
	"body", "blockquote", "details", "fieldset", "figure", "td"
);

declare function _:sectioning-root($node as node()*) as node()*
{
	$node[local-name() = $_:sectioning-root-elements]
};

(:~
 : Sectioning content
 :
 : Elements belonging to the sectioning content model
 : create a section in the current outline that defines
 : the scope of <header> elements, <footer> elements,
 : and heading content.
 :
 : @see https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Sectioning_content
 :)
declare variable $_:sectioning-content-elements as xs:string* := (
 	"body", "article", "aside", "nav", "section" 
);
declare function _:sectioning-content($node as node()*) as node()*
{
	$node[local-name() = $_:sectioning-content-elements]
};

(:~
 : HTML empty elements
 :
 : @see https://developer.mozilla.org/en-US/docs/Glossary/Empty_element
 :)
declare variable $_:empty-elements as xs:string* := (
	"br", "img", "input", "hr", "wbr", "link", "meta", "track", "param", "area", "command", "col", "base", "source", "keygen"	
);

declare function _:empty($node as node()*) as node()* {
	$node/self::*[local-name() = $_:empty-elements or self::*:col[@span]]
};

(:~
 : Heading content
 :
 : Heading content defines the title of a section,
 : whether marked by an explicit sectioning content element or
 : implicitly defined by the heading content itself.
 :)
declare variable $_:heading-content-elements as xs:string* := (
	 "h1", "h2", "h3", "h4", "h5", "h6", "hgroup"
);

declare function _:heading-content($node as node()*) as node()*
{
	$node[local-name() = $_:heading-content-elements]
};

(:~
 : Check headings
 :)
declare function _:heading($node as node()*) as node()*
{
	$node[local-name() = ("h1", "h2", "h3", "h4", "h5", "h6")]
};

(:~
 : Phrasing content
 : 
 : Phrasing content defines the text and the mark-up it contains.
 : Runs of phrasing content make up paragraphs.
 :
 : @see https://developer.mozilla.org/en-US/docs/Web/HTML/Content_categories#Phrasing_content 
 :)
declare variable $_:phrasing-content-elements as xs:string* := (
	 "abbr", "audio", 
	 "b", "bdo", "br", "button", 
	 "canvas", "cite", "code", "command", 
	 "datalist", "dfn", 
	 "em", "embed", 
	 "i", "iframe", "img", "input", 
	 "kbd", "keygen", 
	 "label", 
	 "mark", "math", "meter", 
	 "noscript", 
	 "object", "output", 
	 "progress", 
	 "q", 
	 "ruby",
	 "samp", "script", "select", "small", "span", "strong", "sub", "sup", "svg", 
	 "textarea", "time", 
	 "var", "video", 
	 "wbr"
);

declare function _:phrasing-content($node as node()*) as node()*
{
	for $n in $node return
		typeswitch($n)
	  		case text() return $n
		  	case element() return
			  	let $name := local-name($n)
			  	return if ($name = $_:phrasing-content-elements) then $n else
				switch ($name)
					case "a"
					case "del"
					case "ins"
					case "map"
						return if (_:contains-only-phrasing-content($n)) then $n else ()
					case "area"
						return $n[ancestor::*:map]
					case "link"
					case "meta"
						return $n[@itemprop]
					default
						return ()
	  		default return ()
};

declare function _:contains-only-phrasing-content($el as element()) as xs:boolean
{
	every $node in $el/node() satisfies _:phrasing-content($node)
};

(:~
 : Converts any html to phrasing content
 :
 : If non-phrasing content is encountered it's nodes get unwrapped
 :)
declare function _:reduce-to-phrasing-content($nodes as node()*) as node()*
{
	for $n in $nodes return
		if (_:phrasing-content($n)) then $n
		else _:reduce-to-phrasing-content($n/node())
};

(:~
 : Flow content
 :
 : Elements belonging to the flow content category typically contain text or embedded content.
 :
 : @see https://developer.mozilla.org/en-US/docs/Web/HTML/Content_categories#flow_content
 :)
declare variable $_:flow-content-elements as xs:string* := (
	"a", "abbr", "address", "article", "aside", "audio", "b", "bdo", 
	"blockquote", "br", "button", 
	"canvas", "cite", "code", "command", 
	"datalist", "del", "details", "dfn", "div", "dl", 
	"em", "embed", 
	"fieldset", "figure", "footer", "form", 
	"h1", "h2", "h3", "h4", "h5", "h6", "header", "hgroup", "hr", 
	"i", "iframe", "img", "input", "ins", 
	"kbd", "keygen", 
	"label", 
	"map", "mark", "math", "menu", "meter", 
	"nav", "noscript", 
	"object", "ol", "output", 
	"p", "pre", "progress", 
	"q", 
	"ruby", 
	"samp", "script", "section", "select", "small", "span", "strong", "sub", "sup", "svg", 
	"table", "textarea", "time", 
	"ul", 
	"var", "video", 
	"wbr"
);

declare function _:flow-content($node as node()*) as node()*
{
	for $n in $node return
		typeswitch($n)
			case text() return $n
			case element() return
				let $name := local-name($n)
				return if ($name = $_:flow-content-elements) then $n else
				switch ($name)
					case "area" return $n[ancestor::*:map]
					case "link"
					case "meta" return $n[@itemprop]
					case "style" return $n[@scoped]
					default return ()
		  	default return ()
};

(:~
 : Metadata content
 : 
 : Elements belonging to the metadata content category modify the presentation or 
 : the behavior of the rest of the document, set up links to other documents, or 
 : convey other out of band information.
 :
 : @see https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Metadata_content
 :)
declare variable $_:metadata-content-elements as xs:string* := (
	 "base", "command", "link", "meta", "noscript", "script", "style", "title"
);

(:~
 : Interactive content
 :
 : Interactive content includes elements that are specifically designed for user interaction.
 :
 : @see https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Interactive_content
 :)
declare variable $_:interactive-content-elements as xs:string* := (
	"a", "button", "details", "embed", "iframe", "keygen", "label", "select", "textarea"
);

declare function _:interactive-content($node as node()*) as node()*
{
	for $n in $node return
		typeswitch($n)
		  	case element() return
			  	let $name := local-name($n)
	        	return
	        	if($name = $_:interactive-content-elements) then $n else
				switch($name)
					case "audio"
					case "video"
						return $n[@controls]
					case "img"
					case "object"
						return $n[@usemap]
					case "input"
						return $n[@type]
					case "menu"
						return $n[@type]
					default
						return ()
		  	default return ()
};

(: https://developer.mozilla.org/en-US/docs/HTML/Inline_elements :)
declare variable $_:inline-elements as xs:string* := (
	"a", "abbr", "acronym", 
	"b", "bdo", "br", "big", "button", 
	"cite", "code", 
	"dfn", 
	"em", 
	"i", "img", "input", 
	"kbd", 
	"label", 
	"map", 
	"object", 
	"q", 
	"samp", "strong", "small", "script", "span", "sub", "sup", "select", 
	"tt", "textarea",
	"var"
);

declare variable $_:block-elements as xs:string* := (
	"address", "article", "aside", "audio", 
	"blockquote", 
	"canvas", 
	"dd", "div", "dl", 
	"fieldset", "figcaption", "figure", "figcaption", "footer", "form", 
	"h1", "h2", "h3", "h4", "h5", "h6", "header", "hgroup", "hr", 
	"noscript", "ol", "output", 
	"p", "pre", "section", 
	"table", "tfoot", 
	"ul", 
	"video"
);

declare variable $_:edits-elements as xs:string* := (
	"del", "ins"
);

declare variable $_:list-elements as xs:string* := (
	"dl", "ul", "ol", "li", "dd", "dt"
);

declare variable $_:table-elements as xs:string* := (
	"caption", "col", "colgroup",
	"table", "tbody", "td", "tfoot", "th", "thead", "tr"
);

declare variable $_:form-elements as xs:string* := (
	"button",
	"datalist",
	"form", "fieldset",
	"input",
	"keygen",
	"label",
	"meter",
	"optgroup", "output", "output",
	"progress",
	"select",
	"textarea"
);

declare variable $_:embedded-elements as xs:string* := (
	"embed", "iframe", "object", "param", "source"
);

declare variable $_:document-metadata-elements as xs:string* := (
	"base", "head", "link", "meta", "style", "title"
);

declare variable $_:text-content-elements as xs:string* := (
	"blockquote",
	"dd", "div", "dl", "dd",
	"figcaption",
	"figure",
	"hr",
	"li",
	"main",
	"ol",
	"p",
	"pre",
	"ul"
);

declare variable $_:inline-text-elements as xs:string* := (
	"a", "abbr",
	"b", "bdi", "bdo", "br",
	"cite", "code", 
	"data", "dfn",
	"em", "i",
	"kbd",
	"mark",
	"q",
	"rp", "rt", "ruby",
	"s", "samp", "small", "span", "strong", "sub", "sup", 
	"time",
	"u",
	"var",
	"wbr"
);

declare variable $_:media-elements as xs:string* := (
	"area", "audio",
	"img",
	"map",
	"track",
	"video"
);

(:~
 : Embedded content
 :
 : Embedded content imports another resource or inserts content
 : from another mark-up language or namespace into the document. 
 :
 : @see https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Embedded_content
 :)
declare variable $_:embedded-content-elements as xs:string* := (
	"audio", "canvas", "embed", "iframe", "img", "math", "object", "svg", "video"
);

declare function _:embedded-content($node as node()*) as node()*
{
	$node[local-name() = $_:embedded-content-elements]
}; 


declare variable $_:scripting-elements as xs:string* := (
	"script", "canvas", "noscript"
);

declare variable $_:web-component-elements as xs:string* := (
	"content", "decorator", "element", "shadow", "template"
);

declare variable $_:widget-elements as xs:string* := (
	"details", "dialog",
	"menu", "menuitem",
	"summary"
);

declare variable $_:deprecated-elements as xs:string* := (
	"acronym", "applet",
	"basefont", "big", "blink",
	"center", "command",
	"dir",
	"frame", "frameset",
	"isindex",
	"listing",
	"noembed",
	"plaintext",
	"spacer", "strike",
	"tt",
	"xmp"
);

declare function _:block($node as node()*) as node()*
{
	$node[local-name() = $_:block-elements or self::*:math[@display="block"]]
};

declare function _:inline($node as node()*) as node()*
{
	$node[local-name() = $_:inline-elements or self::*:math[not(@display = "block")]]
};

declare function _:has-class($el as element(), $class as xs:string*) as xs:boolean
{
  let $classnames := tokenize($el/@class, " ")
  let $lookfor := for $c in $class return tokenize($c, " ")
  return every $c in $lookfor satisfies $c = $classnames
};

declare function _:strip-ns($items as item()*) as item()*
{
  for $item in $items return
    typeswitch($item)
      case element()
        return element {QName("", local-name($item))} {$item/@*, _:strip-ns($item/node())}
      case document-node()
        return document {_:strip-ns($item/node())}
      default
      	return $item
};


(:~
 : Serialize html compacting whitespace
 :)
declare function _:stringify($nodes as node()*) as xs:string
{
    let $params := map {
    	"method" : "html",
    	"indent" : "no"
    }
    let $html := _:strip-ns(<html>{_:compact-whitespace($nodes)}</html>)
    let $text := serialize($html, $params)
    return replace($text, "^<html>|</html>$", "")
};

(:~
 : Transformation function to compact white-space in html.
 :
 : Does not affect whitespace in <pre> elements
 :)
declare function _:compact-whitespace($nodes as node()*) as node()*
{
    for $node in $nodes
        return

        if($node instance of element()) then 
            element {QName(namespace-uri($node), local-name($node))}
            {$node/@*, _:compact-whitespace($node/node())}
            
        else if($node instance of text()) then
            if($node/ancestor::*:pre) then $node
            else
                let $next-node := ($node/following-sibling::node())[1]
                let $prev-node := ($node/preceding-sibling::node())[last()]
                let $pad-start := local-name($prev-node) = $_:inline-elements and matches($node, "^\p{Z}")
                let $pad-end := local-name($next-node) = $_:inline-elements and matches($node, "\p{Z}$")
                return text {
                    if($pad-start) then "" else (),
                    normalize-space($node),
                    if($pad-end) then "" else ()
                }
        else $node
};

declare function _:sanitize($html as node()*, $options as map(*)*) as node()* {
	copy $output  := <_>{$html}</_>
	modify (
		if (map:contains($options, 'exclude-tags')) then 
			delete node $output//*[local-name(.) = $options('exclude-tags')] else (),
		if (map:contains($options, 'include-tags')) then
			delete node $output//*[not(local-name(.) = $options('include-tags'))] else (),
		if (map:contains($options, 'exclude-attr')) then
			delete node $output//@*[local-name(.) = $options('exclude-attr')] else (),
		if (map:contains($options, 'include-attr')) then
			delete node $output//@*[not(local-name(.) = $options('include-attr'))] else (),
		if (map:contains($options, 'rename-tags')) then
			for $n in $output//*[local-name(.) = map:keys($options('rename-tags'))] return
				rename node $n as map:get($options('rename-tags'), local-name($n))
			else (),
		if (map:contains($options, 'explode-tags')) then
			for $n in $output//*[local-name(.) = map:keys($options('explode-tags'))] return
				replace node $n with $n/node()
			else ()
			
	)
	return $output/node()
};

declare function _:non-empty-texts($nodes as node()*) as text()*
{
	$nodes//text()[not( . = "")]
};

declare function _:normalize-space($nodes as node()*) as node()*
{
	copy $cp := <_>{ $nodes }</_>
	modify (
		for $txt in _:non-empty-texts($cp)
			return
      if ($txt/ancestor::*:pre) then () else
      replace node $txt with text {replace($txt, "[\p{Z}\n]+", " ")}
	)
	return $cp/node()
};
