module namespace _ = "utils/tests/string";
import module namespace str = "utils/string" at "../string.xqm";

declare %unit:test function _:test-repeat()
{
	unit:assert-equals(str:repeat("a", 5), "aaaaa")
};

declare %unit:test function _:test-pad()
{
   unit:assert-equals(str:pad("test", 8), "test    ")
};

declare %unit:test function _:test-pad-with-padding()
{
   unit:assert-equals(str:pad("test", "a", 8), "testaaaa")
};


declare %unit:test function _:test-length()
{
	let $nodes := (
		text {"aaa"},
		<a>bbb</a>,
		<b>aaa<c>ppp<d>aaa</d></c>ddd<e/>eee</b>)
	return
	(
		unit:assert-equals(str:length($nodes), 21),
		unit:assert-equals(str:length($nodes//self::*), 27)
	)
};

declare %unit:test function _:test-excerpt()
{
	unit:assert-equals(str:excerpt("aaa bbb ccc ddd", 12), "aaa bbb ccc")
};


declare %unit:test function _:test-trim()
{
	unit:assert-equals(str:trim("  aaa "), "aaa")
};
